FROM ubuntu:16.04

ARG SCALA_VERSION=2.12
ARG KAFKA_VERSION=0.10.2.1
ARG ADVERTISED_HOST=localhost

# TODO: replace this URL with one that points to nexus when authentication
# issues are resolved
ARG BINARY_URL=http://apache.osuosl.org/kafka/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz

ADD start-kafka.sh /usr/bin/start-kafka.sh

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install openjdk-8-jre
RUN apt-get -y install wget
RUN apt-get -y autoremove
RUN apt-get -y autoclean

RUN wget ${BINARY_URL}

RUN tar zxvf kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz
RUN mv kafka_${SCALA_VERSION}-${KAFKA_VERSION} /opt/kafka

RUN sed -i.bak "s|#advertised.listeners=PLAINTEXT://your.host.name:9092|advertised.listeners=PLAINTEXT://${ADVERTISED_HOST}:9092|" /opt/kafka/config/server.properties

EXPOSE 9092

CMD ["start-kafka.sh"]
