#!/bin/bash

$KAFKA_HOME/bin/kafka-simple-consumer-shell.sh \
    --broker-list localhost:9092 \
    --topic test
