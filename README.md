Copyright &copy; 2017 Kirk Rader

# Kafka Docker Image

Scripts and documentation for [parasurolophus/kafka](https://hub.docker.com/r/parasaurolophus/kafka/) -- <https://hub.docker.com/r/parasaurolophus/kafka/>

Obtain the pre-built image using:

    docker pull parasaurolophus/kafka

This repository contains the Docker file and associated scripts to create [Docker](https://www.docker.com) containers with a simple [Kafka](http://kafka.apache.org) test environment.

See _Dockerfile_ and _start-kafka.sh_ for the Docker image configuration.

> The image thus created is configured using the [Quickstart](http://kafka.apache.org/quickstart) instructions at <http://kafka.apache.org/quickstart> running on Ubuntu 16.04.
>
> I.e. it is suitable for setting up _ad hoc_ development and testing  environments, not intended for production deployments.

See _build-image.sh_, _create-container.sh_ and _run-container.sh_ for convenience scripts to build the image, create containers from the image and run those containers.

See _start-console-consumer.sh_ for a convenience script to run a console consumer for testing purposes.

See _start-console-producer.sh_ for a matching convenience script to run a console producer.
