#!/bin/bash

KAFKA_HOME=/opt/kafka

KAFKA_PID=0

term_handler() {
  echo 'Stopping Kafka....'
  if [ $KAFKA_PID -ne 0 ]; then
    kill -s TERM "$KAFKA_PID"
    wait "$KAFKA_PID"
  fi
  echo 'Kafka stopped.'
  exit
}

trap "term_handler" SIGHUP SIGINT SIGTERM

# package zookeeper and kafka together for testing and debugging
# convenience
$KAFKA_HOME/bin/zookeeper-server-start.sh \
    $KAFKA_HOME/config/zookeeper.properties &

sleep 2

$KAFKA_HOME/bin/kafka-server-start.sh \
    $KAFKA_HOME/config/server.properties &

KAFKA_PID=$!

wait
